<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 *
 *
 */

$babInstallPath = 'vendor/ovidentia/ovidentia/ovidentia/';
$GLOBALS['babInstallPath'] = $babInstallPath;

require_once $GLOBALS['babInstallPath'].'utilit/addonapi.php';
require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
$session = bab_getInstance('bab_Session');
/*@var $session bab_Session */
$session->setStorage(new \bab_SessionMockStorage());

bab_charset::setDbCharset('utf8');

define('FUNC_WIDGETS_PHP_PATH', dirname(__FILE__).'/../../vendor/ovidentia/widgets/programs/widgets/');
define('FUNC_WIDGETS_JS_PATH', dirname(__FILE__).'/../../vendor/ovidentia/widgets/skins/ovidentia/templates/');

require_once $GLOBALS['babInstallPath'].'utilit/functionality.mock.class.php';

class bab_functionality extends \bab_functionalityMock
{
    public static function includefile($path)
    {
        switch($path) {
            case 'Publication':
                require_once dirname(__FILE__).'/../../programs/api.class.php';
                return 'Func_Publication';
                
            case 'LibOrm':
                require_once dirname(__FILE__).'/../../vendor/ovidentia/liborm/programs/orm.class.php';
                return 'Func_LibOrm';
                
            case 'Widgets':
                require_once dirname(__FILE__).'/../../vendor/ovidentia/widgets/programs/widgets.php';
                
                return 'Func_Widgets';
    
        }
    
        return parent::includefile($path);
    }
    
    
    /**
     * Returns the specified functionality object.
     *
     * If $singleton is set to true, the functionality object will be instanciated as
     * a singleton, i.e. there will be at most one instance of the functionality
     * at a given time.
     *
     * @param	string	$path		The functionality path.
     * @param	bool	$singleton	Whether the functionality should be instanciated as singleton (default true).
     * @return	bab_functionality	The functionality object or false on error.
     */
    public static function get($path, $singleton = true)
    {
        self::includeOriginalDirname($path);
        $classname = self::includefile($path);
    
        if (!$classname) {
            return false;
        }
    
        if ($singleton) {
            return bab_getInstance($classname);
        }
    
        return new $classname();
    }
}



require_once $GLOBALS['babInstallPath'].'utilit/controller.class.php';


function translate($str)
{
    return $str;
}


/**
 * @return \Func_Publication
 */
function api()
{
    $api = bab_functionality::get('Publication');
    $api->loadOrm();

    return $api;
}