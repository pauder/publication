��    A      $  Y   ,      �     �  
   �     �     �     �     �     �  
             "     2     9     F  .   R     �     �     �     �     �     �     �     �     �     �          )     @     L     Q     Y     i     w     �     �     �  
   �     �     �     �          #  
   (  ,   3  	   `     j     y  >   �     �  !   �  5   	     G	      e	  7   �	  7   �	     �	  6   
  A   K
  2   �
     �
  ,   �
     �
          -  )   F  .  p     �     �     �  "   �                    0     9     N  	   d     n     �  1   �     �     �     �     �               (     /     F  :   T  #   �  #   �     �     �     �     �     
          $     C  $   Z          �  $   �     �  *   �            ,   !  	   N     X     l  O   �  #   �  %   �  K     .   j     �  <   �  ;   �     /  <   I  L   �  U   �  !   )  1   K  #   }  #   �  $   �  ,   �     (   3       =      <          0                    .                &   :   9   @   1   *      8      )       ,       	   $      /            -   >             "       5   '   7                      A   4              !   %   #       ;   
          +   6      ?                              2                                 %s is mandatory + New "%s" A draft has been created. A draft has been updated. Access denied Cancel Create publication Created on Data structure Data structures Delete Delete draft Description Do you really want to delete this publication? Documents in %s Draft removed. Dynamic record Edit Edit publication Email title Language List of publications Mail launch. Missing mailing list. Missing publication id Missing structure name Modified on Name No data Number of items OVML template Publication Publication content (%s) Publication removed. Publications by structures Recipients Save Search in all text fields Select display template Select mailing lists Send Send email Sequence not found in XSD: %s, using path %s Structure Structure name Structured publication The "%s" item has not been deleted because it is not yet saved The data structure is empty: %s The display template is mandatory The document has been updated since you start editing The send template is missing. The structure %s does not exists The structure does not allow less than %d items in "%s" The structure does not allow more than %d items in "%s" The structure does not exists This will remove the current draft of this publication This will remove the publication and all linked draft and history This will send this publication to all recipients. Title is missing. Type is provided by the subnode "simpleType" missing publication id missing structure name publications in database the type attribute is not necessary on %s Project-Id-Version: publication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-09-07 15:44+0200
PO-Revision-Date: 2017-09-07 15:46+0200
Last-Translator: Paul de Rosanbo <paul.derosanbo@cantico.fr>
Language-Team: Cantico <paul.derosanbo@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: translate;translate:1,2
X-Generator: Poedit 2.0.3
X-Poedit-SearchPath-0: programs
 %s est obligatoire + Nouveau "%s" Un brouillon à été créé. Un brouillon à été mis à jour. Accès refusé Annuler Créer une publication Crée le Structure de donnés Structures de donnés Supprimer Suprrimer le brouillon Description Voulez-vous vraiment supprimer cette publication? Documents dans %s Brouillon supprimé. Dynamic record Modifier Modifier la publication Titre du mail Langue Liste des publications Mail envoyé. Au moins une liste de diffusion doit être sélectionnée. l'ID de la publication est manquant Le nom de la structure est manquant Modifié le Nom Pas de donnés Nombre d'éléments Template OVML Publication Contenu de la publication (%s) Publication supprimé. Publication par structure de donnés Destinataires Enregistrer Recherche dans tout les champs texte Choisissez le template d'envoi Choisissez au moins une liste de diffusion Envoyer Envoyer un email Sequence not found in XSD: %s, using path %s Structure Nom de la structure Publication structurée Le l'élément "%s" n'a pas été supprimé car il n'est pas encore enregistré La structure de donnés %s est vide Le template d'envoie est obligatoire. Le document à été modifié depuis que vous avez commencé à le modifier Le template d'envoie n'a pas été renseigné. La structure %s n'existe pas La structure n'autorise pas moins de %d éléments dans "%s" La structure n'autorise pas plus de %d éléments dans "%s" La structure n'existe pas Cela va supprimer le brouillon courrant de cette publcation. Cela va suprimer la publication, tous ses brouillons et tous son historique. Cela va envoyer la publication à toutes les listes de diffusion choisies, procéder? Aucun titre n'a été renseigné. Le type est fourni par le sous-noeud "simpleType" L'id de la publication est manquant Le nom de la structure est manquant Publications dans la base de donnés l'attribut type n'est pas nécessaire sur %s 