<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

// @codingStandardsIgnoreStart


function publication_onDeleteAddon()
{

    $addon = bab_getAddonInfosInstance('publication');

    $addon->unregisterFunctionality('Publication');
    $addon->unregisterFunctionality('SitemapEditorNode/Publication');
    
    $addon->unregisterFunctionality('Ovml/Container/Publication');
    $addon->unregisterFunctionality('Ovml/Function/GetPublicationId');
    $addon->unregisterFunctionality('Ovml/Function/PublicationNode'); // deprecated
    $addon->unregisterFunctionality('Ovml/Function/DownloadUrl');

    $addon->unregisterFunctionality('PortletBackend/Publication');
    

    $addon->removeAllEventListeners();

    return true;
}



function publication_upgrade()
{

    require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/omlincl.php';
    require_once dirname(__FILE__).'/functions.php';

    bab_functionality::includeOriginal('SitemapEditorNode');
    bab_functionality::includeOriginal('Ovml/Container');
    bab_functionality::includeOriginal('Ovml/Function');
    bab_functionality::includeOriginal('PortletBackend');

    $addon = bab_getAddonInfosInstance('publication');
    $addon->registerFunctionality('Publication', 'api.class.php');
    $addon->registerFunctionality('SitemapEditorNode/Publication', 'sitemapeditornode.class.php');

    $api = \Ovidentia\Publication\api();


    $synchronize = new bab_synchronizeSql();
    $synchronize->addOrmSet($api->pubNodeSet());
    $synchronize->addOrmSet($api->usedStructureSet());
    $synchronize->updateDatabase();


    $addon->addEventListener('bab_eventBeforeSiteMapCreated', '\Ovidentia\Publication\onBeforeSiteMapCreated', 'utilit/events.php');
    $addon->addEventListener('bab_eventSearchRealms', '\Ovidentia\Publication\onSearchRealms', 'utilit/events.php');

    // old file location
    $addon->unregisterFunctionality('Func_Ovml_Container_Publication');
    $addon->unregisterFunctionality('Func_Ovml_Function_PublicationNode'); // deprecated
    
    // ovml
    $addon->registerFunctionality('Func_Ovml_Container_Publication', 'ovml/ocpublication.class.php');
    $addon->registerFunctionality('Func_Ovml_Function_GetPublicationId', 'ovml/ofgetpublicationid.class.php');
    //$addon->registerFunctionality('Func_Ovml_Function_PublicationNode', 'ovml/ofgetpublicationid.class.php'); // deprecated
    $addon->registerFunctionality('Func_Ovml_Function_DownloadUrl', 'ovml/ofdownloadurl.class.php');
    
    // portlet
    $addon->registerFunctionality('Func_PortletBackend_Publication', 'portlets/portletbackend.class.php');
    
    // editlinks
    $addon->registerFunctionality('Func_ContextActions_Publication', 'contextactions/publication.class.php');
    $addon->registerFunctionality('Func_ContextActions_PublicationNode', 'contextactions/node.class.php');
    
    foreach($api->xsd()->getStructures() as $structure) {
        bab_installWindow::message(sprintf('<strong>%s</strong>', bab_toHtml($structure->getDescription())));
        $structure->updateDatabase(true);
        $structure->setUsedRows();
    }

    return true;
}

// @codingStandardsIgnoreEnd
