<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 *
 *
 */
namespace Ovidentia\Publication;


require_once dirname(__FILE__).'/mock/functions.php';
require_once dirname(__FILE__).'/../programs/utilit/datastructure.class.php';



class SectionTest extends \PHPUnit_Framework_TestCase
{
    
    /**
     * @return DataStructure
     */
    protected function getStructure()
    {
        $path = new \bab_Path(dirname(__FILE__), '..', 'programs', 'structures', 'publication_multiple.xsd');
        return new DataStructure($path);
    }
    
    /**
     *
     * @return FieldSet
     */
    protected function getDuplicableSet()
    {
        $structure = $this->getStructure();
        return $structure->getSequenceFromPath(".//xs:element[@name='diapo']");
    }
    
    public function testDefaultOccurs()
    {
        $set = $this->getDuplicableSet();
        $occurs = $set->getSessionOccurs(array());
        $this->assertEquals(array(0), $occurs);
    }
    
    public function testAddSection()
    {
        $set = $this->getDuplicableSet();
        $set->addSessionOccurs(array());
        
        
        $occurs = $set->getSessionOccurs(array());
        $this->assertEquals(array(0,1), $occurs);
    }
    
    public function testRemoveSection()
    {
        $set = $this->getDuplicableSet();
        $set->removeSessionOccurs(0, array());
        
        $occurs = $set->getSessionOccurs(array());
        $this->assertEquals(array(1), $occurs);
    
    }
    
    
    public function testAddMoreSections()
    {
        $set = $this->getDuplicableSet();
        $set->addSessionOccurs(array());
        $set->addSessionOccurs(array());
        $set->addSessionOccurs(array());
    
        $occurs = $set->getSessionOccurs(array());
        $this->assertEquals(array(1,2,3,4), $occurs);
    }
    
    
    public function testRemoveSection2()
    {
        $set = $this->getDuplicableSet();
        $set->removeSessionOccurs(2, array());
    
        $occurs = $set->getSessionOccurs(array());
        $this->assertEquals(array(1,2,4), $occurs);
    
    }
}
