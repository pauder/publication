<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\Publication;

class PublicationEdit extends \Widget_Form
{
    /**
     *
     * @var DataStructure
     */
    private $structure = null;

    /**
     * @var int
     */
    private $idRecord = null;

    /**
     * @var int
     */
    private $idDraft = null;

    /**
     * @var DynamicDraft|false
     */
    private $draft = null;


    /**
     * @var PubNode
     */
    private $pubnode = null;



    public function __construct(
        DataStructure $structure,
        $idRecord = null,
        $idDraft = null,
        PubNode $pubnode = null,
        $id = null,
        \Widget_Layout $layout = null
    ) {
        $W = \bab_Widgets();

        if (null === $layout) {
            $layout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        }

        parent::__construct($id, $layout);

        $this->structure = $structure;
        $this->idRecord = $idRecord;
        $this->idDraft = $idDraft;
        $this->pubnode = $pubnode;

        $this->setReloadPersistent();

        $this->setName('publication');
        $this->addClass('BabLoginMenuBackground');
        $this->addClass('widget-bordered');

        $this->setHiddenValue('tg', bab_rp('tg'));
        if (isset($_SERVER['HTTP_REFERER'])) {
            $this->setHiddenValue('publication[back]', $_SERVER['HTTP_REFERER']);
        } else {
            $this->setHiddenValue('publication[back]', '');
        }

        $this->setHiddenValue('publication[date]', date('Y-m-d H:i:s'));
        if ($this->pubnode) {
            $this->setHiddenValue('publication[pubnode]', $this->pubnode->id);
        }
        $this->setHiddenValue('publication[structure]', $this->structure->getName());


        $this->addFields();
        $this->addButtons();

        $this->loadValues();
    }

    /**
     * @return \Widget_Link
     */
    protected function getDeleteDraftLink(DynamicDraft $draft)
    {
        $W = \bab_Widgets();

        $controller = api()->controller();
        $structure = $this->structure->getName();

        return $W->Link(
            $W->Icon(
                translate('Delete draft'),
                \Func_Icons::ACTIONS_EDIT_DELETE
            ),
            $controller->Publication()->deleteDraft($draft->id, $structure)
        )
        ->setConfirmationMessage(translate('This will remove the current draft of this publication'));
        /*->setAction($controller->Publication()->deleteDraft($draft->id, $structure))
        ->setSuccessAction($controller->Publication()->back())
        ->setFailedAction($controller->Publication()->edit($structure, $this->idRecord))
        ->setLabel();*/
    }

    /**
     * @return \Widget_Link
     */
    protected function getDeleteLink()
    {
        $W = \bab_Widgets();

        $controller = api()->controller();
        $structure = $this->structure->getName();

        return $W->Link(
            $W->Icon(
                translate('Delete'),
                \Func_Icons::ACTIONS_EDIT_DELETE
            ),
            $controller->Publication()->deletePublication($this->idRecord, $structure)
        )
        ->setConfirmationMessage(translate('This will remove the publication and all linked draft and history'));
        /*->setAction($controller->Publication()->deletePublication($this->idRecord, $structure))
        ->setSuccessAction($controller->Publication()->back())
        ->setFailedAction($controller->Publication()->edit($structure, $this->idRecord))
        ->setLabel();*/
    }

    /**
     * @return \Widget_Link
     */
    protected function getCancelLink()
    {
        $W = \bab_Widgets();

        $controller = api()->controller();

        return $W->Link(
            $W->Icon(
                translate('Cancel'),
                \Func_Icons::ACTIONS_DIALOG_CANCEL
            ),
            $controller->Publication()->back()
        );
    }


    protected function addButtons()
    {
        $W = bab_Widgets();
        /* @var $I Func_Icons */
        $I = \bab_functionality::get('Icons');
        if ($I) {
            $I->includeCss();
        }

        $controller = api()->controller();
        $structure = $this->structure->getName();
        $draft = $this->getDraft();

        $buttons = $W->FlowItems(
            $W->SubmitButton()->validate()
            ->setAction($controller->Publication()->save())
            ->setSuccessAction($controller->Publication()->back())
            ->setFailedAction($controller->Publication()->edit($structure, $this->idRecord, $this->idDraft))
            ->setLabel(translate('Save'))/*,
            $W->SubmitButton()
            ->setAction($controller->Publication()->saveDraft())
            ->setSuccessAction($controller->Publication()->back())
            ->setFailedAction($controller->Publication()->edit($structure, $this->idRecord, $this->draft))
            ->setLabel(translate('Save in draft'))*/
        )->addClass(\Func_Icons::ICON_LEFT_16)->setHorizontalSpacing(1, 'em');

        $buttons->addItem($this->getCancelLink());

        if (false !== $draft) {
            $buttons->addItem($this->getDeleteDraftLink($draft));
        };

        if ($this->idRecord) {
            $buttons->addItem($this->getDeleteLink());
        }

        $this->addItem($buttons);
    }


    /**
     * @param DynamicDraftSet $draftSet
     * @return \ORM_Criteria
     */
    private function getDraftCriteria(DynamicDraftSet $draftSet)
    {
        $criteria = $draftSet->modifiedBy->is(bab_getUserId());

        if (isset($this->idRecord)) {
            return $criteria->_AND_($draftSet->dynamicRecord->is($this->idRecord));
        }

        if (isset($this->idDraft)) {
            return $criteria->_AND_($draftSet->id->is($this->idDraft));
        }

        return null;
    }


    /**
     * Get draft using the dynamic record ID or the id draft and for current user
     * @return DynamicDraft|false
     */
    protected function getDraft()
    {
        if (!isset($this->draft)) {
            $draftSet = $this->structure->getDraftSet();
            $this->draft = false;

            if (null !== $criteria = $this->getDraftCriteria($draftSet)) {
                $this->draft = $draftSet->get($criteria);

                if (!isset($this->draft)) {
                    $this->draft = false;
                }
            }
        }


        return $this->draft;
    }





    protected function loadValues()
    {
        $draft = $this->getDraft();

        if (false !== $draft) {
            $this->setValues(array('publication' => $draft->getValues()));
            $this->setHiddenValue('publication[id]', $draft->dynamicRecord);
            $this->setHiddenValue('publication[draft]', $draft->id);
            $this->setHiddenValue('publication[date]', $draft->modifiedOn);


        } elseif ($this->idRecord) {
            $values = $this->structure->getFormValues($this->idRecord);

            $this->setValues($values, array('publication', $this->structure->getName()));

        }

        if (bab_pp('publication')) {
            $this->setValues(array('publication' => bab_pp('publication')));
        }
    }


    protected function addFields()
    {
        $W = bab_Widgets();
        $vbox = $this->structure->getWidgets(array('publication'));
        $this->addItem($W->Frame(null, $vbox)->setName($this->structure->getName()));
    }
}
