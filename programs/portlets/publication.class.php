<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\Publication;

class PortletPublication extends \Widget_Item implements \portlet_PortletInterface
{
    private $portletId = null;

    private $ovmlTemplate = null;

    private $structure = null;

    private $item = null;



    /**
     */
    public function __construct()
    {
        $W = bab_Widgets();

        $this->item = $W->VBoxItems();
    }


    public function getName()
    {
        return get_class($this);
    }


    public function getPortletDefinition()
    {
        require_once dirname(__FILE__) . '/../functions.php';
        require_once dirname(__FILE__).'/publicationdefinition.class.php';
        return new PortletDefinitionPublication();
    }


    /**
     * receive current user configuration from portlet API
     */
    public function setPreferences(array $configuration)
    {
        foreach ($configuration as $name => $value) {
            $this->setPreference($name, $value);
        }
    }



    public function setPreference($name, $value)
    {
        if ($name === 'structure') {
            $this->structure = $value;
        }

        if ($name === 'ovmlTemplate') {
            $this->ovmlTemplate = $value;
        }
    }


    public function setPortletId($id)
    {
        $this->portletId = $id;
    }



    /**
     * @param Widget_Canvas $canvas
     * @ignore
     */
    public function display(\Widget_Canvas $canvas)
    {
        $W = bab_Widgets();

        $publicationId = '';
        if (strpos($this->structure, '/') !== false) {
            list($this->structure, $publicationId) = explode('/', $this->structure);
        }

        if (empty($this->structure)) {
            return '';
        }
        if ($publicationId == 0) {
            try {
                $xsd = api()->xsd();
                $ds = $xsd->getStructureByName($this->structure);
                $ds->updateDatabase();
                $recordSet = $ds->getRecordSet();
                $record = $recordSet->newRecord();
                $record->save();
                $publicationId = $record->id;

                \Func_PortletBackend::setPreference(
                    $this->portletId,
                    'structure',
                    $this->structure . '/' . $publicationId
                );

            } catch (Exception $e) {
                return '';
            }
        }

        $ovmlArgs = array(
            'Node' => '',
            'Structure' => $this->structure,
            'Publication' => $publicationId,
        );

        if (empty($this->ovmlTemplate)) {
            $this->ovmlTemplate = 'structures/' . $this->structure . '.ovml';
        }
        $layout = $W->Html(bab_printOvmlTemplate($this->ovmlTemplate, $ovmlArgs));

        $display = $layout->display($canvas);

        return $display;
    }
}
