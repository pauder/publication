<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\Publication;

class MailEdit extends \Widget_Form
{
    /**
     *
     * @var DataStructure
     */
    private $structure = null;

    /**
     * @var int
     */
    private $idRecord = null;

    /**
     * @var int
     */
    private $idDraft = null;

    /**
     * @var DynamicDraft|false
     */
    private $draft = null;


    /**
     * @var PubNode
     */
    private $pubnode = null;



    public function __construct($structure, $idRecord)
    {
        $W = \bab_Widgets();

        $layout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');

        parent::__construct(null, $layout);

        $this->structure = $structure;
        $this->idRecord = $idRecord;

        $this->setReloadPersistent();

        $this->setName('mailing');
        $this->addClass('BabLoginMenuBackground');
        $this->addClass('widget-bordered');

        $this->setHiddenValue('tg', bab_rp('tg'));
        if (isset($_SERVER['HTTP_REFERER'])) {
            $this->setHiddenValue('mailing[back]', $_SERVER['HTTP_REFERER']);
        } else {
            $this->setHiddenValue('mailing[back]', '');
        }

        $this->setHiddenValue('mailing[date]', date('Y-m-d H:i:s'));
        $this->setHiddenValue('mailing[structure]', $structure->getName());
        $this->setHiddenValue('mailing[id]', $idRecord);

        $this->addFields();
        $this->addButtons();

        $mailing = bab_pp('mailing', '');
        if ($mailing) {
            $this->setValues($mailing, array('mailing'));
        }
    }

    /**
     * @return \Widget_Link
     */
    protected function getCancelLink()
    {
        $W = \bab_Widgets();

        $controller = api()->controller();

        return $W->Link(
            $W->Icon(
                translate('Cancel'),
                \Func_Icons::ACTIONS_DIALOG_CANCEL
            ),
            $controller->Publication()->back()
        );
    }

    protected function addButtons()
    {
        $W = \bab_Widgets();
        /* @var $I Func_Icons */
        $I = \bab_functionality::get('Icons');
        if ($I) {
            $I->includeCss();
        }

        $controller = api()->controller();
        $structure = $this->structure;

        $buttons = $W->FlowItems(
            $W->SubmitButton()
            ->setConfirmationMessage(translate('This will send this publication to all recipients.'))
            ->validate()
            ->setAction($controller->Publication()->mailSend())
            ->setSuccessAction($controller->Publication()->back())
            ->setFailedAction($controller->Publication()->mail($structure->getName(), $this->idRecord))
            ->setLabel(translate('Send'))
        )->setVerticalAlign('middle')->addClass(\Func_Icons::ICON_LEFT_16)->setHorizontalSpacing(1, 'em');

        $buttons->addItem($this->getCancelLink());

        $this->addItem($buttons);
    }

    protected function templates()
    {
        $W = \bab_Widgets();

        $skin = \bab_skin::getUserSkin();
        $path = $skin->getThemePath();

        $select = $W->Select()->setName('template')
            ->setMandatory(true, translate('The display template is mandatory'))
            ->addOption('', '');

        $babPath = new \bab_Path($path, 'ovml', 'structures');
        $mailName = $this->structure->getName().'_mail';

        foreach ($babPath as $ovml) {
            $ovmlPath = $ovml->tostring();
            if (false !== strpos($ovmlPath, $mailName) && substr($ovmlPath, -5) === '.ovml') {
                $select->addOption(
                    str_replace(
                        array($path,'ovml/'),
                        '',
                        $ovml->tostring()
                    ),
                    str_replace(
                        '.ovml',
                        '',
                        basename($ovml->tostring())
                    )
                );
            }
        }

        return $W->Section(
            translate('Select display template'),
            $select
        );
    }

    protected function mailingListsLayout()
    {
        $W = \bab_Widgets();
        $ML = \bab_functionality::getOriginal('MailingList');
        /*@var $ML \Func_MailingList */

        $lists = $ML->getUsableLists();

        $layout = $W->FlowItems()->setVerticalSpacing(1, 'em')->setHorizontalSpacing(1, 'em');

        foreach ($lists as $id => $name) {
            $subscribers = $ML->getListSubscriptions($id);
            $layout->addItem(
                $W->LabelledWidget(
                    $name . ' ('.translate('Recipients') . ': ' . count($subscribers).')',
                    $W->CheckBox()->setCheckedValue($id)->setName(array('list', $id))
                )
            );
        }

        return $W->Section(
            translate('Select mailing lists'),
            $layout
        );
    }

    protected function title()
    {
        $W = \bab_Widgets();
        return $W->Section(
            translate('Email title'),
            $W->LineEdit()->setSize(60)->setName('title')
        );
    }

    protected function addFields()
    {
        $this->addItem(
            $this->title()
        );
        $this->addItem(
            $this->templates()
        );
        $this->addItem(
            $this->mailingListsLayout()
        );
    }
}
