<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\Publication;

/**
 * Custom result object
 * @package search
 */
class SearchResult extends \bab_SearchResult
{

    /**
     *
     * @var ORM_Iterator|DynamicRecord[]
     */
    private $results;

    public function setIterator(\ORM_Iterator $results)
    {
        $this->results = $results;
    }


    /**
     * @return \bab_SearchRecord | false
     */
    public function current()
    {

        $dynamicRecord = $this->results->current();
        /*@var $dynamicRecord DynamicRecord */

        $record = new \bab_SearchRecord();
        $record->setRealm($this->getRealm());
        
        $structure = $dynamicRecord->getStructure();

        $record->id = $dynamicRecord->id;
        $record->createdOn = $dynamicRecord->createdOn;
        $record->modifiedOn = $dynamicRecord->modifiedOn;
        $record->structure = $structure->getName();
        $record->dynamicRecord = $dynamicRecord;
        
        $fset = $structure->getSequence();
        foreach ($fset->getFields() as $field) {
            $prop = $field->getName();
            $record->$prop = $dynamicRecord->$prop;
        }

        return $record;
    }






    /**
     * @return string
     */
    public function key()
    {
        return $this->results->key();
    }

    public function next()
    {
        $this->results->next();
    }

    public function rewind()
    {
        $this->results->rewind();
    }

    public function count()
    {
        return $this->results->count();
    }

    public function seek($index)
    {
        return $this->results->seek($index);
    }

    /**
     * @return boolean
     */
    public function valid()
    {
        return $this->results->valid();
    }



    /**
     * @return \Widget_Displayable_Interface
     */
    protected function getBreadCrumbs($baseNodeId, \bab_Node $targetNode = null)
    {
        $W = bab_Widgets();
        
        if (!isset($targetNode)) {
            return $W->Label('');
        }
        
        $flow = $W->FlowLayout();
        $flow->addClass('search-result-breadcrumb');
        $flow->setHorizontalSpacing(.3, 'em');
        $node = $targetNode;
        $crumb = null;
        
        do {
            $sitemapItem = $node->getData();
            /*@var $sitemapItem \bab_sitemapItem */
            
            if (!$sitemapItem->breadCrumbIgnore) {
                if (isset($crumb)) {
                    $flow->addItem($W->Html('&rarr;')->addClass('arrow'), 0);
                }
            
                $url = $sitemapItem->getRwUrl();
                if (empty($url)) {
                    $crumb = $W->Label($sitemapItem->name);
                } else {
                    $crumb = $W->Link($sitemapItem->name, $url);
                }
                
                $flow->addItem($crumb, 0);
            }
            
            $node = $node->parentNode();
        } while ($node->getId() !== $baseNodeId
            && $sitemapItem->getRewriteName() !== \bab_siteMap::REWRITING_ROOT_NODE);
        
        return $flow;
    }




    /**
     * Get a view of search results as HTML string
     * The items to display are extracted from the <code>bab_SearchResult</code> object,
     * the display start at the iterator current position and stop after $count elements
     *
     * @param   int                 $count      number of items to display
     *
     * @return string
     */
    public function getHtml($count)
    {

        $W = bab_Widgets();
        $vbox = $W->VBoxLayout()->setVerticalSpacing(1.5, 'em');
        
        $sitemap = \bab_siteMap::getSiteSitemap();
        $baseNodeId = $sitemap->getVisibleRootNode();

        while ($this->valid() && 0 < $count) {
            $count--;
            $record = $this->current();
            $dynamicRecord = $record->dynamicRecord;

            $vbox->addItem(
                $W->VBoxItems(
                    $this->getBreadCrumbs($baseNodeId, $dynamicRecord->getCustomNode()),
                    $dynamicRecord->getRecordClickablePreview()
                )->addClass('publication-search-result')
            );

            $this->next();
        }

        return $vbox->display($W->HtmlCanvas());
    }
}
