<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\Publication;

/**
 * ORM Record set
 * all sets are extended from this class
 *
 * @property \ORM_PkField $id
 * @property \ORM_DatetimeField $createdOn
 * @property \ORM_DatetimeField $modifiedOn
 * @property \ORM_UserField $createdBy
 * @property \ORM_UserField $modifiedBy
 */
class RecordSet extends \ORM_MySqlRecordSet
{

    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_DatetimeField('createdOn'),
            ORM_DatetimeField('modifiedOn'),
            ORM_UserField('createdBy'),
            ORM_UserField('modifiedBy')
        );
    }



    public function save(\ORM_Record $record)
    {

        if (empty($record->id) && empty($record->createdBy)) {
            $record->createdBy = bab_getUserId();
        }

        if (empty($record->id) && empty($record->createdOn)) {
            $record->createdOn = date('Y-m-d H:i:s');
        }

        $record->modifiedBy = bab_getUserId();
        $record->modifiedOn = date('Y-m-d H:i:s');

        return parent::save($record);
    }
}
