<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\Publication;

class CtrlPublication extends Controller
{

    /**
     * Generic list of publication
     * @param string $structure
     */
    public function displayList($structure = null, $search = null)
    {
        if (!bab_isUserAdministrator()) {
            throw new \bab_SaveException(translate('Access denied'));
        }

        $W = bab_Widgets();

        $structure = api()->xsd()->getStructureByName($structure);
        $list = api()->ui()->publicationList();

        $set = $structure->getRecordSet();
        $structure->updateDatabase();

        $list->setDefaultColumns($set);

        $filter = null;
        if (isset($search['filter'])) {
            $filter = $search['filter'];
        }

        $criteria = $list->getFilterCriteria($filter);

        $res = $set->select($criteria);
        $res->orderDesc($set->createdOn);

        $list->setDataSource($res);


        $page = $W->BabPage();
        $page->setTitle($structure->getDescription());

        $page->addItem($list->filterPanel());

        return $page;
    }


    /**
     * Get editor fragment for a structure path
     * @param string $structure     Structure name
     * @param string $path          XPath in the XSD for the duplicable fieldSet
     * @param array $widgetNamePath    name path of the widget, including positions of sections for duplicables sections
     */
    public function reloadFieldSet($structure = null, $path = null, $widgetNamePath = null)
    {
        $structure = api()->xsd()->getStructureByName($structure);
        /*@var $structure DataStructure */
        return $structure->getSectionWidget($path, $widgetNamePath);
    }


    /**
     * Get editor fragment for a structure path
     * @param string $structure     Structure name
     * @param string $path          XPath in the XSD for the duplicable fieldSet
     * @param array $widgetNamePath Name path in editor
     */
    public function addSection($structure = null, $path = null, $widgetNamePath = null)
    {
        $structure = api()->xsd()->getStructureByName($structure);
        $set = $structure->getSequenceFromPath($path);
        $set->addSessionOccurs($widgetNamePath);

        die();
    }

    /**
     * Get editor fragment for a structure path
     * @param string $structure     Structure name
     * @param string $path          XPath in the XSD for the duplicable fieldSet
     * @param int    $position
     */
    public function removeSection($structure = null, $path = null, $position = null, $widgetNamePath = null)
    {
        $structure = api()->xsd()->getStructureByName($structure);
        /*@var $structure DataStructure */

        $set = $structure->getSequenceFromPath($path);
        $set->removeSessionOccurs($position, $widgetNamePath);

        die();
    }


    /**
     * Display edit form
     * @param string $structure     structure name
     * @param string $id            id record
     */
    public function edit($structure = null, $id = null, $pubnode = null)
    {
        if (!canEdit()) {
            throw new \bab_AccessException(translate('Access denied'));
        }

        $pubNodeRecord = null;
        if (isset($pubnode)) {
            $pubNodeSet = api()->pubNodeSet();
            /*@var $pubNodeSet PubNodeSet */
            $pubNodeRecord = $pubNodeSet->get($pubnode);

            if (!$pubNodeRecord->canCreateDynamicRecord()) {
                throw new \bab_AccessException(translate('Access denied'));
            }

            // force structure via pubNode

            $structure = $pubNodeRecord->structure;
        }



        if (!isset($structure)) {
            throw new \bab_AccessException(translate('missing structure name'));
        }

        $W = \bab_Widgets();
        $structure = api()->xsd()->getStructureByName($structure);
        /*@var $structure DataStructure */

        $page = $W->BabPage();

        if (!empty($id)) {
            $page->setTitle($structure->getEditTitle());
        } else {
            $page->setTitle($structure->getCreateTitle());
        }


        $editor = api()->ui()->publicationEdit($structure, $id, null, $pubNodeRecord);
        $page->addItem($editor);

        return $page;
    }

    /**
     * Display edit form
     * @param string $structure     structure name
     * @param string $id            id draft
     */
    public function editDraft($structure = null, $id = null)
    {
        if (!canEdit()) {
            throw new \bab_AccessException(translate('Access denied'));
        }
        if (!isset($structure)) {
            throw new \bab_AccessException(translate('missing structure name'));
        }

        $W = \bab_Widgets();

        $page = $W->BabPage()->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));

        $editor = api()->ui()->publicationEdit($structure, null, $id);
        $page->addItem($editor);

        return $page;
    }


    /**
     * Set back url to session
     * @param string $back
     */
    protected function setBack($back)
    {
        require_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
        $session = bab_getInstance('bab_Session');
        $session->publication_back = $back;
    }



    /**
     * Save the document
     * remove my draft(s) attached to document
     */
    public function save($publication = null)
    {
        if (!canEdit()) {
            throw new \bab_AccessException(translate('Access denied'));
        }

        if (!isset($publication['structure'])) {
            throw new \bab_AccessException(translate('missing structure name'));
        }

        $api = api();
        $xsd = $api->xsd();
        $structure = $xsd->getStructureByName($publication['structure']);
        $dynSet = $structure->getRecordSet();
        $structure->updateDatabase();


        $babBody = bab_getBody();

        $idDoc = null;

        $structureName = $structure->getName();
        if (!isset($publication[$structureName])) {
            throw new \bab_SaveException(translate('No data'));
        }

        if (!empty($publication[$structureName]['id'])) {
            $idDoc = $publication[$structureName]['id'];

            $record = $dynSet->get($idDoc);
            if ($record->modifiedOn > $publication['date']) {
                $e = new \bab_SaveException(translate('The document has been updated since you start editing'));
                $e->redirect = false;
                throw $e;
            }


            $babBody->addNextPageMessage('Document updated');
        } else {
            $babBody->addNextPageMessage('New document created');
        }


        $record = $structure->saveRecord($publication[$structureName], $idDoc);

        $record->deleteDraft();


        $this->setBack($publication['back']);

        return true;
    }


    /**
     * Save document as draft
     * @param array $publication
     */
    public function saveDraft($publication = null)
    {
        if (!canEdit()) {
            throw new \bab_AccessException(translate('Access denied'));
        }
        if (!isset($publication['structure'])) {
            throw new \bab_AccessException(translate('missing structure name'));
        }

        $xsd = api()->xsd();
        $structure = $xsd->getStructureByName($publication['structure']);
        $draftSet = $structure->getDraftSet();
        $structure->updateDatabase();


        $record = false;
        if (isset($publication['draft']) && $publication['draft']) {
            $record = $draftSet->get(
                $draftSet->id->is($publication['draft'])
                ->_AND_($draftSet->modifiedBy->is(bab_getUserId()))
            );
        }

        if (!$record) {
            $record = $draftSet->newRecord();
            bab_getBody()->addNextPageMessage(translate('A draft has been created.'));
        } else {
            bab_getBody()->addNextPageMessage(translate('A draft has been updated.'));
        }

        $record->setValues($publication);
        $record->save();

        $this->setBack($publication['back']);
        return true;
    }


    /**
     * Delete one draft
     */
    public function deleteDraft($draft = null, $structure = null)
    {
        if (!canEdit()) {
            throw new \bab_AccessException(translate('Access denied'));
        }

        $xsd = api()->xsd();
        $structure = $xsd->getStructureByName($structure);

        $draftSet = $structure->getDraftSet();
        $draftSet->delete(
            $draftSet->id->is($draft)
            ->_AND_($draftSet->modifiedBy->is(bab_getUserId()))
        );

        bab_getBody()->addNextPageMessage(translate('Draft removed.'));

        return $this->back();
    }


    /**
     * Delete one publication
     */
    public function deletePublication($id = null, $structure = null)
    {
        if (!canEdit()) {
            throw new \bab_AccessException(translate('Access denied'));
        }

        $xsd = api()->xsd();
        $structure = $xsd->getStructureByName($structure);

        $draftSet = $structure->getDraftSet();
        $draftSet->delete(
            $draftSet->dynamicRecord->is($id)
        );

        $historySet = $structure->getHistorySet();
        $historySet->delete(
            $historySet->dynamicRecord->is($id)
        );

        $recordSet = $structure->getRecordSet();
        $recordSet->delete(
            $recordSet->id->is($id)
        );

        bab_getBody()->addNextPageMessage(translate('Publication removed.'));

        return $this->back();
    }

    public function back()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
        $session = bab_getInstance('bab_Session');

        if (isset($session->publication_back) && '' !== $session->publication_back) {
            $url = new \bab_url($session->publication_back);
            $url->location();
        }

        // If no referer
        $W = bab_Widgets();
        $page = $W->BabPage();
        $page->addItem($W->Html('
            <script type="text/javascript">
                history.go(-2);
            </script>
        '));


        return $page;
    }



    /**
     * Display mailing campaign
     * @param string $structure     structure name
     * @param string $id            id draft
     */
    public function mail($structure = null, $id = null)
    {
        if (!isset($id)) {
            throw new \bab_AccessException(translate('Missing publication id'));
        }
        if (!isset($structure)) {
            throw new \bab_AccessException(translate('Missing structure name'));
        }
        $structure = api()->xsd()->getStructureByName($structure);

        if (!isset($structure)) {
            throw new \bab_AccessException(translate('The structure does not exists'));
        }

        $set = $structure->getRecordSet();
        $publication = $set->get($id);

        if (!$publication->canSendMail()) {
            throw new \bab_AccessException(translate('Access denied'));
        }

        $W = \bab_Widgets();

        $page = $W->BabPage()->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));

        $editor = api()->ui()->mailEdit($structure, $id);
        $page->addItem($editor);

        return $page;
    }



    /**
     * Display Mailing launch confirm
     */
    public function mailSend($mailing = null)
    {
        if (!isset($mailing['id']) || !$mailing['id']) {
            throw new \bab_AccessException(translate('missing publication id'));
        }
        if (!isset($mailing['structure']) || !$mailing['structure']) {
            throw new \bab_AccessException(translate('missing structure name'));
        }

        $M = \bab_functionality::get('Mailing');
        $ML = \bab_functionality::getOriginal('MailingList');
        if (!$M || !$M->isConfigured() || !$ML || !$ML->canUseMailingList()) {
            throw new \bab_AccessException(translate('Access denied'));
        }

        if (!isset($mailing['title']) || $mailing['title'] === '') {
            $e = new \bab_SaveException(translate('Title is missing.'));
            $e->redirect = false;
            throw $e;
        }

        if (!isset($mailing['template']) || $mailing['template'] === '') {
            $e = new \bab_SaveException(translate('The send template is missing.'));
            $e->redirect = false;
            throw $e;
        }

        if (!isset($mailing['list']) || !is_array($mailing['list']) || count($mailing['list']) == 0) {
            $e = new \bab_SaveException(translate('Missing mailing list.'));
            $e->redirect = false;
            throw $e;
        }

        $lists = $ML->getUsableLists();
        foreach ($mailing['list'] as $list) {
            if ($list) {
                if (!isset($lists[$list])) {
                    throw new \bab_AccessException(translate('Access denied'));
                }
            }
        }

        $ovmlArgs = array(
            'Node' => '',
            'Structure' => $mailing['structure'],
            'Publication' => $mailing['id'],
        );

        $mail = $M->getMailing();

        $content = bab_printOvmlTemplate($mailing['template'], $ovmlArgs);
        $mail->content = $content;
        $mail->title = $mailing['title'];

        foreach ($mailing['list'] as $list) {
            if ($list) {
                $mail->addMailingList($list);
            }
        }

        $M->send($mail);

        bab_getBody()->addNextPageMessage(translate('Mail launch.'));

        $this->setBack($mailing['back']);

        return true;
    }
}
