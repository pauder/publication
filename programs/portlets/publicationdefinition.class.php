<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\Publication;

class PortletDefinitionPublication implements \portlet_PortletDefinitionInterface
{

    /**
     * @var bab_addonInfos $addon
     */
    protected $addon;


    public function __construct()
    {
        $this->addon = bab_getAddonInfosInstance('publication');
    }


    public function getId()
    {
        return 'Publication';
    }

    public function getName()
    {
        return translate('Publication');
    }


    public function getDescription()
    {
        return translate('List of publications');
    }


    public function getPortlet()
    {
        require_once dirname(__FILE__).'/publication.class.php';
        return new PortletPublication();
    }



    /**
     * Static-cached list of publication structures
     * @return string[][]
     */
    private static function getStructures()
    {
        static $structures = null;

        if (!isset($structures)) {
            $xsd = api()->xsd();
            $xsdStructures = $xsd->getStructures();
            $structures = array();

            foreach ($xsdStructures as $xsdStructure) {
                $structureName = $xsdStructure->getName();

                $structures[] = array(
                    'value' => $structureName . '/0',
                    'label' => sprintf(translate('+ New "%s"'), $xsdStructure->getDescription()),
                    'group' => $xsdStructure->getDescription()
                );

                $ds = $xsd->getStructureByName($structureName);
                $ds->updateDatabase();

                /* @var $dynSet \ORM_MySqlRecordSet */
                $dynSet = $ds->getRecordSet();

                $records = $dynSet->select();
                foreach ($records as $record) {
                    $label = $record->getRecordTitle();
                    if (empty($label)) {
                        $label = '[' . $record->id . ']';
                    }
                    $structures[] = array(
                        'value' => $structureName . '/' . $record->id,
                        'label' => $label,
                        'group' => $xsdStructure->getDescription()
                    );
                }
            }
        }

        return $structures;
    }



    /**
     * @return array
     */
    public function getPreferenceFields()
    {
        $structures = self::getStructures();

        $preferenceFields = array(
            array(
                'type' => 'list',
                'label' => translate('Structure'),
                'name' => 'structure',
                'options' => $structures
            ),
            array(
                'type' => 'string',
                'label' => translate('OVML template'),
                'name' => 'ovmlTemplate'
            )
        );


        return $preferenceFields;
    }


    /**
     * Returns the widget rich icon URL.
     * 128x128 ?
     *
     * @return string
     */
    public function getRichIcon()
    {
        return $this->addon->getStylePath() . 'images/publication.png';
    }


    /**
     * Returns the widget icon URL.
     * 16x16 ?
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->addon->getStylePath() . 'images/publication.png';
    }

    /**
     * Get thumbnail URL
     * max 120x60
     */
    public function getThumbnail()
    {
        return '';
    }

    public function getConfigurationActions()
    {
        return array();
    }
}
