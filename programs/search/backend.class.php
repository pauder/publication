<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\Publication;

class SearchBackend extends \bab_SearchBackEnd
{
    /**
     * @var \ORM_RecordSet
     */
    public $set = null;
    
    /**
     * (non-PHPdoc)
     * @see utilit/bab_SearchBackEnd#andCriteria($oLeftCriteria, $oRightCriteria)
     *
     *
     * @return bab_PeriodCriteria
     */
    public function andCriteria(\bab_SearchCriteria $oLeftCriteria, \bab_SearchCriteria $oRightCriteria)
    {
        $left = $oLeftCriteria->toString($this);
    
        if (!($left instanceof \ORM_Criteria)) {
            throw new \Exception('transformation failed for leftCriteria');
        }
    
        $right = $oRightCriteria->toString($this);
    
        if (!($right instanceof \ORM_Criteria)) {
            throw new \Exception('transformation failed for rightCriteria');
        }
    
        return $left->_AND_($right);
    }
    
    
    /**
     * @todo not implemented in bab_PeriodCriteria
     *
     * (non-PHPdoc)
     * @see bab_SearchBackEnd::orCriteria()
     */
    public function orCriteria(\bab_SearchCriteria $oLeftCriteria, \bab_SearchCriteria $oRightCriteria)
    {
        $left = $oLeftCriteria->toString($this);
    
        if (!($left instanceof \ORM_Criteria)) {
            throw new \Exception('transformation failed for leftCriteria');
        }
    
        $right = $oRightCriteria->toString($this);
    
        if (!($right instanceof \ORM_Criteria)) {
            throw new \Exception('transformation failed for rightCriteria');
        }
        
        return $left->_OR_($right);
    }
    
    /**
     * @todo not implemented in bab_PeriodCriteria
     *
     * (non-PHPdoc)
     * @see bab_SearchBackEnd::notCriteria()
     */
    public function notCriteria(\bab_SearchCriteria $oCriteria)
    {
        $ormCriteria = $oCriteria->toString($this);
        /*@var $ormCriteria \ORM_Criteria */
        return $ormCriteria->_NOT();
    }
    

    
    
    /**
     * (non-PHPdoc)
     * @see utilit/bab_SearchBackEnd#in($oField, $mixedValue)
     *
     *
     * @return \ORM_Criteria | null
     */
    public function in(\bab_SearchField $oField, $mixedValue)
    {
        $prop = $oField->getName();
        $field = $this->set->$prop;
        
        return $field->in($mixedValue);
        
    }
    
    
    public function is(\bab_SearchField $oField, $mixedValue)
    {
        $prop = $oField->getName();
        $field = $this->set->$prop;
        
        return $field->like($mixedValue);
    }
    
    
    
    public function contain(\bab_SearchField $oField, $sValue)
    {
        $prop = $oField->getName();
        $field = $this->set->$prop;
        
        return $field->contains($sValue);
    }
    
    
    
    public function greaterThanOrEqual(\bab_SearchField $oField, $sValue)
    {
        $prop = $oField->getName();
        $field = $this->set->$prop;
        
        return $field->greaterThanOrEqual($sValue);
    }
    
    
    public function lessThanOrEqual(\bab_SearchField $oField, $sValue)
    {
        $prop = $oField->getName();
        $field = $this->set->$prop;
        
        return $field->lessThanOrEqual($sValue);
    }
}
