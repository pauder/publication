
; <?php
// @codingStandardsIgnoreStart
/*
[general]
name="publication"
version="0.3.5"
addon_type="EXTENSION"
mysql_character_set_database="latin1,utf8"
encoding="UTF-8"
description="Structured publication"
description.fr="Module de publication de contenus associés à une structure de données"
delete=1
longdesc=""
ov_version="8.4.94"
php_version="5.3.0"
addon_access_control=0
author="Cantico ( support@cantico.fr )"
icon="icon.png"
tags="extension,publication"

[addons]
LibOrm=">=0.9.15"
widgets=">=1.0.100"
portlets=">=0.18"
sitemap_editor=">=0.7.9"

;*/
// @codingStandardsIgnoreStart
?>