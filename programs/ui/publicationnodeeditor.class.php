<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\Publication;

class PublicationNodeEditor extends \smed_NodeEditor
{
    
    /**
     * DataStructure
     */
    protected $dataStructure;
    
    
    /**
     * @var \Widget_Section
     */
    protected $publicationSection;
    
    public function __construct($contentType, \smed_Node $node = null)
    {
        $structureName = mb_substr($contentType, mb_strlen('datastructure/'));
        $xsd = api()->xsd();
        
        $this->dataStructure = $xsd->getStructureByName($structureName);
        
        
        parent::__construct('publication_node_editor', null, $node);
        $this->setHiddenValue('node[type]', $contentType);
        
        $this->setReloadPersistent();

        // Add fields from datastructure
        
        
        
        $this->advancedSection->addItem($this->Url(), 0);
        $this->advancedSection->addItem($this->Description(), 0);
        $this->generalSection->addItem($this->Label(), 0);
        
        
        
        
        
        
        
    }
    
    
    /**
     * Insert sections into form
     */
    protected function addSections()
    {
        $W = bab_Widgets();
        $this->publicationSection = $W->Section(
            sprintf(translate('Publication content (%s)'), $this->dataStructure->getDescription()),
            $W->VBoxLayout()->setVerticalSpacing(1, 'em')
        );
        
        $this->publicationSection->setFoldable(true);
        
        $vbox = $this->dataStructure->getWidgets(array('node'));
        $frame = $W->Frame(null, $vbox)->setName($this->dataStructure->getName());
        $this->publicationSection->addItem($frame);
        
        
        $this->addItem($this->generalSection);
        $this->addItem($this->publicationSection);
        
        $this->referencingSection->setFoldable(true, true);
        
        $this->addItem($this->referencingSection);
        $this->addItem($this->advancedSection);
    }
}
