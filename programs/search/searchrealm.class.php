<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\Publication;

class SearchRealm extends \bab_SearchRealm
{
    /**
     * @var DataStructure
     */
    public $structure;
    
    
    public function getName()
    {
        return 'publication_'.$this->structure->getName();
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see bab_SearchTestable::getDescription()
     */
    public function getDescription()
    {
        return $this->structure->getDescription();
    }

    
    
    /**
     * Get the list of all available searching locations
     * This method return an array where keys are mixed (each realm can have his specific location identifiers)
     * and the values are internationalized descriptions of the searching locations
     *
     * @see bab_SearchCriteria
     *
     * @return  array
     */
    public function getAllSearchLocations()
    {
        return array('orm' => translate('publications in database'));
    }
    
    /**
     * Get the list of available sorting methods for this realm
     * this method return an array where keys are constants from the <code>bab_SearchRealm</code> object
     * and the values are internationalized descriptions of the sorting methods
     *
     *
     * @return  array
     */
    public function getSortMethods()
    {
        return array();
    }
    
    
    /**
     * Get a list of available fields
     *
     * @return  array   each values will be a <code>bab_SearchField</code> object
     */
    public function getFields()
    {
        $fields = array(
            $this->createField('id', 'id')->searchable(false),
            $this->createField('createdOn', translate('Created on'))->searchable(false),
            $this->createField('modifiedOn', translate('Modified on'))->searchable(false),
            $this->createField('structure', translate('Structure name'))->searchable(false),
            $this->createField('dynamicRecord', translate('Dynamic record'))->searchable(false),
            $this->createField('sitemaplang', translate('Language'))
        );
        
        $fset = $this->structure->getSequence();
        
        foreach ($fset->getFields() as $field) {
            $ormField = $field->getOrmField();
            switch (get_class($ormField)) {
                case 'ORM_StringField':
                case 'ORM_TextField':
                case 'ORM_EnumField':
                    $fields[] = $this->createField($field->getName(), $field->getDescription());
            }
        }
        
        return $fields;
    }
    
    
    
    /**
     * Test if the current user have access to this search realm.
     * If this method return true, a search with criteria as given
     * by the method <code>getDefaultCriteria</code> must return
     * all items accessibles by the current user.
     *
     * @return  bool
     */
    public function isAccessValid()
    {
        // TODO
        return true;
    }
    
    
    
    /**
     * Get default criteria, may contain access rights verification as criteria for search request
     * This default criteria is valid when the search realm is accessible
     * @see bab_SearchRealm::isAccessValid()
     *
     * @return  bab_SearchCriteria
     */
    public function getDefaultCriteria()
    {
        $criteria = parent::getDefaultCriteria();
        
        // language filter
        $criteria = $criteria->_AND_($this->sitemaplang->in(array('', \bab_getLanguage())));

        return $criteria;
    }
    
    

    
    /**
     * get orm iterator from criteria
     * @return \ORM_Iterator
     */
    protected function orm(\bab_SearchCriteria $criteria)
    {
        
        require_once $GLOBALS['babInstallPath'].'utilit/searchbackend.php';
        require_once dirname(__FILE__).'/backend.class.php';
        
        $backend = new SearchBackend();
        $backend->set = $this->structure->getRecordSet();
        
        $ormCriteria = $criteria->toString($backend);
        
        if (empty($ormCriteria)) {
            $ormCriteria = new \ORM_TrueCriterion();
        }
        
        // add level 1 subsets
        /*
        $fset = $this->structure->getSequence();
        foreach ($fset->getSubSets() as $subFSet) {
            $backend->set = $subFSet->getRecordSet();
            $ormCriteria->_OR_($criteria->toString($backend));
        }
        */
        
        // filter out unaccessibles records
        $ormCriteria = $ormCriteria->_AND_($backend->set->getVisibleCriteria());

        $res = $backend->set->select($ormCriteria);
        
        // bab_debug($res->getSelectQuery());
        return $res;
    }
    
    
    
    /**
     * Search in realm with criteria
     * This method will use the <code>getSearchLocations</code> method to get the search locations
     * and the <code>sort_method</code> property to sort results
     *
     * @param   bab_SearchCriteria      $criteria
     *
     * @return  bab_SearchResult | bab_SearchResultCollection   (iterator)
     */
    public function search(\bab_SearchCriteria $criteria)
    {
        require_once dirname(__FILE__).'/searchresult.class.php';
        
        $result = new SearchResult;
        $result->setRealm($this);

        $locations = $this->getSearchLocations();

        if (isset($locations['orm'])) {
            $iterator = $this->orm($criteria);
            $result->setIterator($iterator);
            return $result;
        }
        
        throw new \Exception('No valid search location');
    }
}
