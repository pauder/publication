<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\Publication;

/**
 * Xsd data structure file reader
 */
class Xsd
{
    
    /**
     * @var array
     */
    private static $structures;
    

    /**
     * Load all data structure from a path
     * @param \bab_Path $path
     * @return DataStructure[]
     */
    public function getFromPath(\bab_Path $path)
    {
        $list = array();

        foreach ($path as $file) {
            /*@var $file \bab_Path */
            if (!$file->isFile()) {
                continue;
            }

            $iPos = mb_strpos($file->getBasename(), ".");
            $ext = mb_strtolower(mb_substr($file->getBasename(), $iPos + 1));
            if ('xsd' !== $ext) {
                continue;
            }

            $name = mb_substr($file->getBasename(), 0, -4);
            $list[$name] = new DataStructure($file);

        }

        return $list;
    }



    /**
     * Load all data structures stored in a skin
     * @param \bab_Skin $skin
     *
     * @return DataStructure[]
     */
    public function getFromSkin(\bab_skin $skin)
    {
        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
        $structuresPath = new \bab_Path($skin->getThemePath(), 'structures');
        return $this->getFromPath($structuresPath);
    }

    /**
     * Load all data structures proposed by other addons via the event
     *
     * @return DataStructure[]
     */
    public function getFromEvent()
    {
        api()->includeStructureEvent();
        $event = new StructureEvent();
        \bab_fireEvent($event);

        return $event->getStructures();
    }


    /**
     * Load all data structures
     *
     * @return DataStructure[]
     */
    public function getStructures()
    {
        if (!isset(self::$structures)) {
            require_once $GLOBALS['babInstallPath'].'utilit/skinincl.php';
            api()->requireFiles();
    
            $skins = \bab_skin::getList();
            $structures = array();
            foreach ($skins as $skin) {
                $structures+= $this->getFromSkin($skin);
            }
            $structures += $this->getFromEvent();
    
            $addon = \bab_getAddonInfosInstance('publication');
    
            if ($addon) {
                $path = new \bab_Path($addon->getPhpPath(), 'structures', 'publication_article.xsd');
                $structures['publication_article'] = new DataStructure($path);
        
                $path = new \bab_Path($addon->getPhpPath(), 'structures', 'publication_test_struct.xsd');
                $structures['publication_test_struct'] = new DataStructure($path);
        
                $path = new \bab_Path($addon->getPhpPath(), 'structures', 'publication_multiple.xsd');
                $structures['publication_multiple'] = new DataStructure($path);
            }
            
            self::$structures = $structures;
        }
        
        return self::$structures;
    }

    /**
     * Get structure by name
     * @throws \Exception
     *
     * @param string $name
     *
     * @return DataStructure
     */
    public function getStructureByName($name)
    {
        $all = $this->getStructures();

        if (!isset($all[$name])) {
            throw new \Exception(sprintf(translate('The structure %s does not exists'), $name));
        }

        return $all[$name];
    }
}
