<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


/**
 * Contextual button for a node (create a publication in this node)
 */// @codingStandardsIgnoreStart
class Func_ContextActions_PublicationNode extends Func_ContextActions
{
    // @codingStandardsIgnoreEnd
    
    public function getDescription()
    {
        return 'Match a node to publish into';
    }

    /**
     * Get a pattern or string to match a CSS class
     * @return string
     */
    public function getClassSelector()
    {
        return '[class*=publication-nodedynamicrecord-]';
    }
    
    
    /**
     * @return array
     */
    protected function getNodeAndStructureFromClasses(array $classes)
    {
        foreach ($classes as $className) {
            $structure = null;
            if (preg_match('/publication-nodedynamicrecord-([^\-]+)-(.+)/', $className, $matches)) {
                $structure = \Ovidentia\Publication\api()->xsd()->getStructureByName($matches[1]);
                
                if (!$structure) {
                    continue;
                }
                
                $sitemap = \bab_sitemap::getFromSite();
                $node = $sitemap->getNodeById($matches[2]);
                
                if (!$node) {
                    continue;
                }
                
                return array($structure, $node);
            }
        }
        
        return null;
    }
    
    /**
     * Get the list of actions
     * @param array $classes all css classes found on the element
     * @param bab_url $url Page url where the actions will be added
     * @return Widget_Action[]
     */
    public function getActions(array $classes, bab_url $url)
    {
        require_once dirname(__FILE__).'/../functions.php';
        $W = \bab_Widgets();
        list($structure, $node) = $this->getNodeAndStructureFromClasses($classes);
        /*@var $structure \Ovidentia\Publication\DataStructure */
        /*@var $node \bab_Node */
        $actions = array();
        
        if (!$structure || !$node) {
            return $actions;
        }

        $sitemapItem = $node->getData();
        /*@var $sitemapItem \bab_SitemapItem */
        
        
        // TODO: The right test does not work, $sitemapItem->canCreate() need to be fixed
        // method from sitemap editor is used to fix the problem
        $smedNode = smed_Node::getFromId(1, $sitemapItem->id_function);
        
        
        if ($smedNode->canCreate()) {
            $title = $structure->getCreateTitle();
            if (empty($title)) {
                $title = \Ovidentia\Publication\translate('Create publication');
            }
            
            $actions[] = $W->Action()
                ->setMethod('addon/sitemap_editor/main', 'node.add', array(
                    'content_type' => 'datastructure/'.$structure->getName(),
                    'parentNode' => $node->getId(),
                    'explorer' => 0,
                    'backurl' => $url->toString()
                ))
                ->setTitle($title)
                ->setIcon(Func_Icons::ACTIONS_ARTICLE_NEW);
        }
        
        return $actions;
    }
}
