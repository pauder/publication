<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


/**
 * Contextual button for a publication (edit or view publication)
 *
 */// @codingStandardsIgnoreStart
class Func_ContextActions_Publication extends Func_ContextActions
{
    // @codingStandardsIgnoreEnd
    
    public function getDescription()
    {
        return 'Match a publication';
    }

    /**
     * Get a pattern or string to match a CSS class
     * @return string
     */
    public function getClassSelector()
    {
        return '[class*=publication-dynamicrecord-]';
    }
    
    /**
     * @return \Ovidentia\Publication\DynamicRecord
     */
    protected function getPubFromClasses(array $classes)
    {

        foreach ($classes as $className) {
            $matches = null;
            if (preg_match('/publication-dynamicrecord-([^\-]+)-(\d+)/', $className, $matches)) {
                $structure = \Ovidentia\Publication\api()->xsd()->getStructureByName($matches[1]);
                $set = $structure->getRecordSet();
                
                return $set->get($matches[2]);
            }
        }
        
        return null;
    }
    
    /**
     * Get the list of actions
     * @param array $classes all css classes found on the element
     * @param bab_url $url Page url where the actions will be added
     * @return Widget_Action[]
     */
    public function getActions(array $classes, bab_url $url)
    {
        require_once dirname(__FILE__).'/../functions.php';
        $W = \bab_Widgets();
        $publication = $this->getPubFromClasses($classes);

        $actions = array();
        
        if (!isset($publication)) {
            bab_debug(sprintf('No publication found from classes: %s', implode(', ', $classes)));
            return $actions;
        }
    
        if ($publication->canUpdate()) {
            $structure = $publication->getStructure();
            $customNode = $publication->getCustomNode();
            
            $title = $structure->getEditTitle();
            if (empty($title)) {
                $title = \Ovidentia\Publication\translate('Edit publication');
            }
            
            if (isset($customNode)) {
                $actions[] = $W->Action()
                    ->setMethod('addon/sitemap_editor/main', 'node.edit', array(
                        'node' => $customNode->getId(),
                        'explorer' => 0,
                        'backurl' => $url->toString()
                    ))
                    ->setTitle($title)
                    ->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT);
                
            } else {
                $actions[] = $W->Action()
                    ->setMethod('addon/publication/main', 'publication.edit', array(
                        'structure' => $publication->getStructure()->getName(),
                        'id' => $publication->id))
                    ->setTitle($title)
                    ->setIcon(Func_Icons::ACTIONS_DOCUMENT_EDIT);
            }
        }
        
        if ($publication->canSendMail()) {
            $actions[] = $W->Action()
                ->setMethod('addon/publication/main', 'publication.mail', array(
                    'structure' => $publication->getStructure()->getName(),
                    'id' => $publication->id))
                ->setTitle(\Ovidentia\Publication\translate('Send email'))
                ->setIcon(Func_Icons::ACTIONS_MAIL_SEND);
        }
        
        return $actions;
    }
}
