<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\Publication;

/**
 * The xsd data structure object base class
 */
class DataStructure
{

    /**
     * @var \bab_Path
     */
    private $xsdFilePath;

    /**
     * @var \DOMDocument
     */
    private $domDocument;


    /**
     * @var \DOMXPath
     */
    private $DOMXpath;

    /**
     * Prefix used in document for the XMLSchema namespace
     * @var string
     */
    private $namespacePrefix;

    /**
     * @var string
     */
    private $description;


    /**
     * @var FieldSet
     */
    private $fields;

    /**
     *
     * @var array
     */
    private $shortNamespaces = array();


    /**
     * If the temporaray data must be deleted to display a form
     * default to false for form sections refreshed via ajax
     *
     * this property will be set to true before genereating the main form
     * to prevent old temporary data to be loaded on a new document
     *
     * @var bool
     */
    private $clearTemporary = false;
    
    
    /**
     * database syncronization status to prevent mutiple tests in the same refresh
     * @var bool
     */
    private $synchronized = false;
    
    /**
     * Name path used as prefix in edit form
     * @var array
     */
    public $namePath;
    

    /**
     * @param \bab_Path $xsdFilePath
     */
    public function __construct(\bab_Path $xsdFilePath)
    {
        if (false !== mb_strpos($xsdFilePath->getBasename(), '-')) {
            // - is used as separator in editlinks classname
            throw new \Exception(
                sprintf('dashs are not allowed in data structures names: %s', $xsdFilePath->tostring())
            );
        }

        $this->xsdFilePath = $xsdFilePath;
    }


    
    /**
     * @return \bab_Path
     */
    public function getPath()
    {
        return $this->xsdFilePath;
    }



    /**
     * Return array of namespaces of the document
     *
     * @param DOMXPath $xpath
     *
     * @return array
     */
    public function getNamespaces($xpath)
    {
        $query   = "//namespace::*";
        $entries =  $xpath->query($query);
        $nspaces = array();
        foreach ($entries as $entry) {
            if ($entry->nodeValue == "http://www.w3.org/2001/XMLSchema") {
                $this->xsdNs = preg_replace('/xmlns:(.*)/', "$1", $entry->nodeName);
            }
            if ($entry->nodeName != 'xmlns:xml') {
                if (preg_match('/:/', $entry->nodeName)) {
                    $nodeName = explode(':', $entry->nodeName);
                    $nspaces[$nodeName[1]] = $entry->nodeValue;
                } else {
                    $nspaces[$entry->nodeName] = $entry->nodeValue;
                }
            }
        }
        return $nspaces;
    }


    /**
     * Load includes in XML Schema
     *
     * @param DOMDocument $dom       Instance of DOMDocument
     * @param string      $filepath
     * @param string      $namespace
     *
     * @return void
     */
    public function loadIncludes($dom, $filepath = '', $namespace = '')
    {
        $xpath = new \DOMXPath($dom);
        $query = "//*[local-name()='include' and namespace-uri()='http://www.w3.org/2001/XMLSchema']";
        $includes = $xpath->query($query);
        foreach ($includes as $entry) {
            $parent = $entry->parentNode;
            $xsd = new \DOMDocument();
            $xsdFileName = realpath($filepath.DIRECTORY_SEPARATOR.$entry->getAttribute("schemaLocation"));
            if (!file_exists($xsdFileName)) {
                continue;
            }
            $result = $xsd->load($xsdFileName, LIBXML_DTDLOAD|LIBXML_DTDATTR|LIBXML_NOENT|LIBXML_XINCLUDE);
            if ($result) {
                $mxpath = new \DOMXPath($xsd);
                $this->shortNamespaces = array_merge($this->shortNamespaces, $this->getNamespaces($mxpath));
            }
            foreach ($xsd->documentElement->childNodes as $node) {
                if ($node->nodeName == $this->xsdNs.":include") {
                    $loc = realpath($filepath.DIRECTORY_SEPARATOR.$node->getAttribute('schemaLocation'));
                    $node->setAttribute('schemaLocation', $loc);
                    if ($this->debug) {
                        print('Change included schema location to '.$loc." \n");
                    }
                    $newNode = $dom->importNode($node, true);
                    $parent->insertBefore($newNode, $entry);
                } else {
                    if ($namespace != '') {
                        $newNodeNs = $xsd->createAttribute("namespace");
                        $textEl = $xsd->createTextNode($namespace);
                        $newNodeNs->appendChild($textEl);
                        $node->appendChild($newNodeNs);
                    }
                    $newNode = $dom->importNode($node, true);
                    $parent->insertBefore($newNode, $entry);
                }
            }
            $parent->removeChild($entry);
        }
        return $dom;
    }


    /**
     * Get data structure unique name
     * In the xsd file the name will be used in the name attribute of the root element
     * the name of the XSD file witohut extension must be the same of the attribute
     *
     * @return string
     */
    public function getName()
    {
        return mb_substr($this->xsdFilePath->getBasename(), 0, -4);
    }


    /**
     * Get DOMDocument object
     * @return \DOMDocument
     */
    protected function getDomDocument()
    {
        if (!isset($this->domDocument)) {
            $this->domDocument = new \DOMDocument();
            $this->domDocument->preserveWhiteSpace = false;
            if (false === $this->domDocument->load($this->xsdFilePath->tostring())) {
                throw new \Exception(sprintf('Failed to load %s', $this->xsdFilePath->tostring()));
            }

            $this->loadIncludes($this->domDocument, dirname($this->xsdFilePath->tostring()));
        }

        return $this->domDocument;
    }


    /**
     * Prefix used in document for the XMLSchema namespace
     * @return string
     */
    protected function getNamespacePrefix()
    {
        if (!isset($this->namespacePrefix)) {
            $schema = $this->getDomDocument()->getElementsByTagNameNS('http://www.w3.org/2001/XMLSchema', 'schema');

            foreach ($schema as $element) {
                $this->namespacePrefix = $element->prefix;
            }
        }

        return $this->namespacePrefix;
    }



    /**
     * Get a DOMXPath object for Xpath request in data structure
     * @return \DOMXPath
     */
    protected function getDOMXpath()
    {
        if (!isset($this->DOMXpath)) {
            $doc = $this->getDomDocument();
            $this->DOMXpath = new \DOMXPath($doc);
        }

        return $this->DOMXpath;
    }



    /**
     * Get the documentation content according to user language
     * @param string $query XPath query to get list of documentation tag with optional xml:lang attribute
     * @return string
     */
    private function getInternationalizedText($query)
    {
        $xpath = $this->getDOMXpath();
        $elements = $xpath->query($query);

        if (!isset($elements)) {
            return null;
        }

        $byLanguage = array();
        foreach ($elements as $node) {
            $value = bab_getStringAccordingToDatabase($node->nodeValue, 'UTF-8');
            $lang = $node->attributes->getNamedItem('xml:lang');

            if (!isset($lang)) {
                return $value;
            }


            // if lang attribute, index nodeValue by language
            $byLanguage[$lang] = $value;
        }

        if (1 === count($byLanguage)) {
            return reset($byLanguage);
        }

        $currentLanguage = bab_getLanguage();
        if (!isset($byLanguage[$currentLanguage])) {
            return reset($byLanguage);
        }

        return $byLanguage[$currentLanguage];
    }

    /**
     * Get data structure description, mandatory in xsd
     * found in schema/element/annotation/documentation node
     *
     * @return string
     */
    public function getDescription()
    {
        if (!isset($this->description)) {
            $xsd = $this->getNamespacePrefix();

            $this->description = $this->getInternationalizedText(
                "/$xsd:schema/$xsd:element/$xsd:annotation/$xsd:documentation"
            );

            if (!isset($this->description)) {
                $message = sprintf(
                    translate('
                        The schema from %s need a /schema/element/annotation/documentation
                        tag for the description of the data structure'),
                    $this->xsdFilePath->getBasename()
                );

                throw new \Exception($message);
            }
        }


        return $this->description;
    }



    /**
     * Get the title to display for create page (optional in xsd)
     * found in schema/annotation/documentation node
     * @return string
     */
    public function getCreateTitle()
    {
        $xsd = $this->getNamespacePrefix();

        return $this->getInternationalizedText(
            "/$xsd:schema/$xsd:annotation/$xsd:documentation[@type=\"create\"]"
        );
    }

    /**
     * Get the title to display for edit page (optional in xsd)
     * found in schema/annotation/documentation node
     * @return string
     */
    public function getEditTitle()
    {
        $xsd = $this->getNamespacePrefix();

        return $this->getInternationalizedText(
            "/$xsd:schema/$xsd:annotation/$xsd:documentation[@type=\"edit\"]"
        );
    }






    /**
     * Get DataStructure field
     * @param \DOMNode $node
     * @return DataStructureField
     */
    protected function getField(\DOMNode $node)
    {
        require_once dirname(__FILE__).'/datastructurefield.class.php';

        return new DataStructureField($node, $this->getDOMXpath(), $this->getNamespacePrefix(), $this);
    }


    /**
     * Get DataStructure field set
     * @param \DOMNode $node
     * @return FieldSet
     */
    protected function getFieldSet(\DOMNode $node)
    {
        require_once dirname(__FILE__).'/datastructurefield.class.php';
        require_once dirname(__FILE__).'/fieldset.class.php';

        return new FieldSet($node, $this->getDOMXpath(), $this->getNamespacePrefix(), $this);
    }




    /**
     * get sequence, cache on fields
     * @return FieldSet
     */
    public function getSequence()
    {

        if (!isset($this->fields)) {
            $xsd = $this->getNamespacePrefix();
            $xpath = $this->getDOMXpath();
            $elements = $xpath->query("/$xsd:schema/$xsd:element");

            if (!isset($elements) || 0 === $elements->length) {
                throw new \Exception(
                    sprintf(
                        translate('The data structure is empty: %s'),
                        $this->xsdFilePath->tostring()
                    )
                );
            }

            $this->fields = $this->getFieldSet($elements->item(0));
            $this->fields->loadFields();
        }

        return $this->fields;
    }


    /**
     * Get sequence, no cache, using a path
     * @see CtrlPublication::section()
     *
     * @param string $path
     *
     * @return FieldSet
     */
    public function getSequenceFromPath($path)
    {
        $xpath = $this->getDOMXpath();
        $elements = $xpath->query($path);

        if (!isset($elements) || 0 === $elements->length) {
            require_once $GLOBALS['babInstallPath'].'utilit/controller.class.php';
            
            throw new \bab_SaveException(
                sprintf(
                    translate('Sequence not found in XSD: %s, using path %s'),
                    $this->xsdFilePath->tostring(),
                    $path
                )
            );
        }

        $fields = $this->getFieldSet($elements->item(0));
        $fields->loadFields();

        return $fields;
    }




    /**
     * Get data structure ORM fields
     * @return \ORM_Field[]
     */
    public function getOrmFields()
    {
        return $this->getRecordSet()->getFields();
    }
    
    
    /**
     * test if temporary data must be deleted before displaying form
     * never delete if on POST (failed submit)
     * @return bool
     */
    public function getClearTemporary()
    {
        return (empty($_POST) && $this->clearTemporary);
    }
    



    /**
     * Get data structure labelled widgets
     * @return \Widget_VBoxLayout
     */
    public function getWidgets(array $namePath = null)
    {
        $set = $this->getSequence();
        
        // clear the number of loaded sub records
        
        require_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
        $session = \bab_getInstance('bab_Session');
        if (isset($session->publication_occurs)) {
            $session->publication_occurs = array();
        }
        
        $this->clearTemporary = true;
        
        $this->namePath = $namePath;
        
        // initialize the widget name path with the prefix
        $widgetNamePath = $namePath;
        $widgetNamePath[] = $this->getName();

        return $set->getWidgets($widgetNamePath, false);
    }

    /**
     * @param string $path         Path in XSD
     * @param array $widgetNamePath      name path used by widget
     */
    public function getSectionWidget($path, array $widgetNamePath)
    {
        $set = $this->getSequenceFromPath($path);
        $set->loadContent();
        
        $W = bab_Widgets();
        $W->includePhpClass('Widget_Section');
        
        require_once dirname(__FILE__).'/../ui/section.class.php';
        
        $duplicableField = $set->getDuplicableField($set);
        $section = new Section($set, $duplicableField, $widgetNamePath);
        
        $section->setName($widgetNamePath);
        
        return $section;
    }


    
    /**
     * @return DynamicRecordSet
     */
    public function getRecordSet()
    {
        $set = $this->getSequence()->getRecordSet();
        
        return $set;
    }
    
    
    /**
     * @return DynamicDraftSet
     */
    public function getDraftSet()
    {
        $set = $this->getSequence()->getDraftSet();

        return $set;
    }
    
    
    /**
     * @return DynamicHistorySet
     */
    public function getHistorySet()
    {
        $set = $this->getSequence()->getHistorySet();

        return $set;
    }
    
    



    /**
     * Synchronize database for all tables using this structure
     * synchronize only if the xsd file has been modified since the last synchronization
     *
     * @param bool $force   Ignore last synchronization date,
     *                      used in addon updgrade program
     *                      but not in application
     */
    public function updateDatabase($force = false)
    {
        if ($this->synchronized) {
            return;
        }
        
        $uset = api()->usedStructureSet();
        $usedStructure = $uset->get($uset->name->is($this->getName()));
        
        
        if (isset($usedStructure) && !$force) {
            $dbTime = bab_mktime($usedStructure->synchronizedOn);
            $fileTime = filemtime($this->xsdFilePath->tostring());
            
            if ($dbTime >= $fileTime) {
                return;
            }
        }
        
        $fset = $this->getSequence();
        $fset->updateDatabase($this->getName());
        
        if (!isset($usedStructure)) {
            $usedStructure = $uset->newRecord();
            $usedStructure->name = $this->getName();
            $usedStructure->path = $this->xsdFilePath->tostring();
        }
        
        $usedStructure->synchronizedOn = date('Y-m-d H:i:s');
        $usedStructure->save();

        $this->synchronized = true;
    }
    
    
    /**
     * used only in upgrade if there are errors
     * ussually number of row will be updated from DynamicSet::save and DynamicSet::delete
     */
    public function setUsedRows()
    {
        $uset = api()->usedStructureSet();
        $usedStructure = $uset->get($uset->name->is($this->getName()));
        
        $set = $this->getRecordSet();
        
        $usedStructure->rows = $set->select()->count();
        $usedStructure->save();
    }
    
    
    
    /**
     * Get array of values to use in edit form
     * This method set occurs values in session (numbers of sub-sections needed for each duplicables field set)
     * @param int $id
     * @return array
     */
    public function getFormValues($id)
    {
        $this->updateDatabase();
        
        $fset = $this->getSequence();
        $set = $fset->getRecordSet();

        $record = $set->get($id);
        
        if (!isset($record)) {
            return array();
        }
        
        return $fset->getFormValues($record);
    }
    
    
    /**
     * Save posted values recursively
     * @param array $post
     * @param int $id
     *
     * @return DynamicRecord
     */
    public function saveRecord(array $post, $id = null)
    {
        $fset = $this->getSequence();
        $set = $this->getRecordSet();
        
        if (!empty($id)) {
            $record = $set->get($id);
        }
        
        if (!isset($record)) {
            $record = $set->newRecord();
        }
        
        
        $fset->saveRecord($post, $record);
        
        require_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
        $session = \bab_getInstance('bab_Session');
        unset($session->publication_occurs);
        
        return $record;
    }
    
    
    /**
     * Set the sitemap language in records identified by $id (in set and sub-sets)
     *
     * @param int $id
     * @param string $language
     */
    public function setSitemapLanguage($id, $language)
    {
        $fset = $this->getSequence();
        $set = $fset->getRecordSet();
        $mainRecord = $set->get($id);
        $mainRecord->sitemaplang = $language;
        $mainRecord->save();
        
        $fset->setSubSetsLanguage(array($id), $language);
    }
    
    /**
     * Test if this structure must be proposed in search engine
     * @return boolean
     */
    public function canSearch()
    {
        $xsd = $this->getNamespacePrefix();
        $xpath = $this->getDOMXpath();
        $rootElementDisabledSearch = $xpath->query("/$xsd:schema/$xsd:element[@search=\"0\"]");
        
        if (!isset($rootElementDisabledSearch) || $rootElementDisabledSearch->length === 0) {
            return true;
        }
        
        return false;
    }
}
