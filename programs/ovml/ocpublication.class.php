<?php
use Ovidentia\Publication;

//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


/**
 * The node attribute is a sitemap node id
 *
 * <OCPublication [node=""] [structure="" [publication=""]] [filter=""] [limit=""] [order=""] [orderby=""]>
 *      <OVPublicationId>
 *      <OVPublication...>
 * </OCPublication>
 *
 */// @codingStandardsIgnoreStart
class Func_Ovml_Container_Publication extends \Func_Ovml_Container
{
    // @codingStandardsIgnoreEnd

    /**
     * @var \Ovidentia\Publication\DynamicRecordSet
     */
    private $recordSet;

    /**
     * @var \ORM_Iterator
     */
    private $res;

    /**
     * @var string
     */
    private $node;

    /**
     * @var string
     */
    private $structure;

    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $publication;

    /**
     * @var string
     */
    private $filter;

    /**
     * @var string
     */
    private $limit;

    /**
     * @var string
     */
    private $order;

    /**
     * @var string
     */
    private $orderby;


    public $attributesInVariables = false;


    public function getAttribute($name)
    {
        if (method_exists($this->ctx->curctx, 'getAttribute')) {
            return $this->ctx->curctx->getAttribute($name);
        }

        // before ovidentia 8.4.93
        return $this->ctx->curctx->get($name);
    }


    /**
     * @return \ORM_TrueCriterion
     */
    protected function getCriteria()
    {
        $criteria = new \ORM_TrueCriterion();

        if ($this->node) {
            $criteria = $this->getCriteriaFromNode();
        }

        if ($this->publication) {
            $dynSet = $this->getRecordSet();
            $arr = explode(',', $this->publication);
            $criteria = $criteria->_AND_($dynSet->id->in($arr));
        }

        if ($this->filter) {
            $dynSet = $this->getRecordSet();
            $filters = explode(',', $this->filter);
            foreach ($filters as $filterCriteria) {
                $aFilter = explode('=', $filterCriteria);
                $col = $aFilter[0];
                $col = $dynSet->$col;
                $criteria = $criteria->_AND_($col->in($aFilter[1]));
            }
        }

        return $criteria;
    }


    public function setOvmlContext(babOvTemplate $ctx)
    {
        parent::setOvmlContext($ctx);

        $this->node = $this->getTarget($this->getAttribute('node'));
        $this->structure = $this->getAttribute('structure');
        $this->path = $this->getAttribute('path');
        $this->publication = $this->getAttribute('publication');
        $this->filter = $this->getAttribute('filter');
        $this->limit = $this->getAttribute('limit');
        $this->order = $this->getAttribute('order');
        $this->orderby = $this->getAttribute('orderby');

        require_once dirname(__FILE__) . '/../functions.php';

        /* @var $api Func_Publication */
        $api = \Ovidentia\Publication\api();
        $api->includePubNodeSet();
        $api->includeDynamicSet();

        try {
            $criteria = $this->getCriteria();
            $dynSet = $this->getRecordSet();
        } catch (Exception $e) {
            // ex: la structure n'existe pas
            bab_getBody()->addError($e->getMessage());
            $this->ctx->curctx->push('CCount', 0);
            return;
        }

        $this->res = $dynSet->select($criteria);

        if ($this->order && $this->order == 'desc') {
            $order = 'orderDesc';
        } else {
            $order = 'orderAsc';
        }

        if ($this->orderby) {
            $orderby = $this->orderby;
        } else {
            if ($this->publication && !$this->path) {
                $dynSet->addFields(
                    $dynSet->id->field(explode(',', $this->publication))->setName('__currendidorder')
                );
                $orderby = '__currendidorder';
            } else {
                $orderby = 'sortkey';
            }
        }

        $this->res->$order($dynSet->$orderby);
        $this->res->$order($dynSet->createdOn);

        if ($this->limit) {
            $this->res->limit($this->limit);
        }


        $this->ctx->curctx->push('PublicationStructure', $this->structure);
        $this->ctx->curctx->push('CCount', $this->res->count());

        $this->res->rewind();
        $this->idx = 0;
    }


    /**
     * Get target node id if possible
     * @return string
     */
    private function getTarget($nodeId)
    {
        if (empty($nodeId)) {
            return $nodeId;
        }

        $sitemap = bab_siteMap::getFromSite();
        $node = $sitemap->getNodeById($nodeId);

        if (!isset($node)) {
            trigger_error(sprintf('Node not found in sitemap "%s"', $nodeId));
            return $nodeId;
        }

        $sitemapItem = $node->getData();
        /*@var $sitemapItem bab_sitemapItem */
        $target = $sitemapItem->getTarget();

        return $target->id_function;
    }


    /**
     * Get record set from structure
     * @return \Ovidentia\Publication\DynamicRecordSet
     */
    private function getRecordSet()
    {
        if (!isset($this->recordSet)) {
            $xsd = \Ovidentia\Publication\api()->xsd();

            if (empty($this->structure)) {
                throw new Exception('Structure is missing in '.$this->ctx->debug_location);
            }

            $ds = $xsd->getStructureByName($this->structure);
            $ds->updateDatabase();

            /*@var $ds \Ovidentia\Publication\DataStructure */

            if (false !== $this->path) {
                $fset = $ds->getSequenceFromPath($this->path);
            } else {
                $fset = $ds->getSequence();
            }

            $this->recordSet = $fset->getRecordSet();
        }



        return $this->recordSet;
    }


    private function getCriteriaFromNode()
    {
        /*
        if ('publicationNode_' === mb_substr($this->node, 0, mb_strlen('publicationNode_'))) {
            return $this->getFromPubNode();
        }
        */

        return $this->getFromPublication();
    }



    private function getFromPublication()
    {
        $explNode = explode('_', $this->node);
        $documentId = end($explNode);

        $this->structure = mb_substr($this->node, mb_strlen('publicationDocument_'), -1* mb_strlen('_'.$documentId));
        $dynSet = $this->getRecordSet();


        return $dynSet->id->is($documentId);
    }


    /**
     * @deprecated
     */
    private function getFromPubNode()
    {
        $pubNotset = \Ovidentia\Publication\api()->pubNodeSet();
        $pubNode = $pubNotset->get($pubNotset->nodeid->is($this->node));

        $this->structure = $pubNode->structure;

        $dynSet = $this->getRecordSet();

        if (!isset($dynSet)) {
            return null;
        }

        return $dynSet->pubnode->is($pubNode->id);
    }


    /**
     *
     */
    public function getnext()
    {
        if (!$this->res) {
            return false;
        }

        if ($this->res->valid()) {
            $record = $this->res->current();
            /*@var $record Ovidentia\Publication\DynamicRecord */

            $this->ctx->curctx->push('PublicationSitemapNodeId', $record->getNodeId());

            $customNodeId = '';
            $customNodeText = '';
            $customNodeDescription = '';
            if ($customNode = $record->getCustomNode()) {
                $customNodeId = $customNode->getId();
                $sitemapItem = $customNode->getData();
                $customNodeText = $sitemapItem->name;
                $customNodeDescription = $sitemapItem->description;
            }

            $this->ctx->curctx->push('PublicationSitemapCustomNodeId', $customNodeId);
            $this->ctx->curctx->push('PublicationSitemapCustomNodeText', $customNodeText);
            $this->ctx->curctx->push('PublicationSitemapCustomNodeDescription', $customNodeDescription);
            $this->ctx->curctx->push('PublicationSitemapUrl', $record->getSitemapUrl());

            $ovml = $record->getOvmlValues();
            $values = $ovml['values'];
            $format = $ovml['format'];

            foreach ($values as $key => $value) {
                $this->ctx->curctx->push($key, $value);
            }

            foreach ($format as $key => $value) {
                $this->ctx->curctx->setFormat($key, $value);
            }

            $this->ctx->curctx->push('CIndex', $this->idx);
            $this->idx++;
            $this->res->next();
            return true;
        } else {
            $this->idx = 0;
            $this->res->rewind();
            return false;
        }
    }
}
