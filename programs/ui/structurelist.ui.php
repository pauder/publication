<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\Publication;

class StructureList extends \Widget_BabTableView
{
    const NAME = 0;
    const DESCRIPTION = 1;
    const ITEMS = 2;
    const LISTITEMS = 3;
    
    protected $row = 0;
    
    public function __construct()
    {
        $I = \bab_functionality::get('Icons');
        if ($I) {
            $I->includeCss();
        }

        parent::__construct();
        
        $this->addClass(\Func_Icons::ICON_LEFT_24);
    }
   
    
    public function setDefaultColumns()
    {
        $W = bab_Widgets();
        
        $this->addHeadRow($this->row);
        
        $this->addItem($W->Label(translate('Name')), $this->row, self::NAME);
        $this->addItem($W->Label(translate('Description')), $this->row, self::DESCRIPTION);
        $this->addItem($W->Label(translate('Number of items')), $this->row, self::ITEMS);
        $this->addItem($W->Label(''), $this->row, self::LISTITEMS);
        
        $this->row++;
    }
    
    protected function addStructure(DataStructure $structure)
    {
        $W = bab_Widgets();
        $publication = api()->controller()->publication();
        $set = $structure->getRecordSet();
        $res = $set->select();
        
        $this->addItem($W->Label($structure->getName()), $this->row, self::NAME);
        $this->addItem($W->Label($structure->getDescription()), $this->row, self::DESCRIPTION);
        $this->addItem($W->Label($res->count()), $this->row, self::ITEMS);
        $this->addItem($W->Link('', $publication->displayList($structure->getName()))
                ->addClass('icon')
                ->addClass(\Func_Icons::ACTIONS_VIEW_LIST_DETAILS), $this->row, self::LISTITEMS);
        
        $this->row++;
    }
    
    public function loadStructures()
    {
        foreach (api()->xsd()->getStructures() as $structure) {
            $this->addStructure($structure);
        }
    }
}
