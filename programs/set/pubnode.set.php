<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\Publication;

/**
 * publication node set
 * Lined to a sitemap node, this record will manage a list of publication with the same data structure
 *
 * @property ORM_PkField $id
 * @property ORM_StringField $structure
 * @property ORM_StringField $nodeid
 * @property ORM_StringField $xsdchecksum
 * @property ORM_DatetimeField $createdOn
 * @property ORM_DatetimeField $modifiedOn
 * @property ORM_UserField $createdBy
 * @property ORM_UserField $modifiedBy
 *
 * @method PubNode newRecord()
 * @method PubNode get(mixed $criteria)
 * @method PubNode[]|\ORM_Iterator select(\ORM_Criteria $criteria)
 */
class PubNodeSet extends RecordSet
{

    public function __construct()
    {
        parent::__construct();

        $this->addFields(
            ORM_StringField('structure')
                ->setDescription('Structure name, this is the root element node name attribute'),
            ORM_StringField('nodeid')
                ->setDescription('Sitemap node ID in the core sitemap')
        );
    }
    
    
    
    public function readableCriteria()
    {
        return new \ORM_TrueCriterion();
    }
}
